/// <reference types="cordova-plugin-file" />
export declare enum FileHelperReadMode {
    text = 0,
    array = 1,
    url = 2,
    binarystr = 3,
    json = 4,
    internalURL = 5,
    fileobj = 6
}
/**
 * Represent informations about a file.
 */
export interface FileStats {
    mtime: number | undefined;
    mdate: Date | undefined;
    size: number;
    name: string;
    type: string;
}
export interface EntryObject {
    [path: string]: Entry[];
}
export interface EntryTree {
    [path: string]: EntryTree | string | null;
}
/**
 * Simplify file access and directory navigation with native Promise support
 */
export declare class FileHelper {
    protected ready: Promise<void> | null;
    protected root: string | null;
    /**
     * Create a new FileHelper object using a root path.
     * Root is automatically set to cordova.file.externalDataDirectory || cordova.file.dataDirectory on mobile devices,
     * and to "cdvfile://localhost/temporary/" on browser.
     *
     * You can create a new FileHelper instance using a FileHelper instance, working directory will be used as root.
     *
     * You can also create a new FileHelper instance using a DirectoryEntry,
     * returned by cordova-plugin-file's callbacks or by FileHelper.mkdir().
     * DirectoryEntry's URL will be used as root.
     *
     * @param root Root directory of the new instance
     */
    constructor(root?: string | FileHelper | DirectoryEntry);
    /**
     * Returns a resolved promise when FileHelper is ready.
     */
    waitInit(): Promise<void>;
    /**
     * Return parent directory of current path. Must NOT have trailing slash !
     * @param path
     */
    protected getDirUrlOfPath(path: string): string;
    /**
     * Return basename (filename or foldername) of current path. Must NOT have trailing slash !
     */
    protected getBasenameOfPath(path: string): string;
    /**
     * Check if path exists (either a file or a directory)
     * @param path
     */
    exists(path: string): Promise<boolean>;
    /**
     * Check if path exists and is a file
     * @param path
     */
    isFile(path: string): Promise<boolean>;
    /**
     * Check if path exists and is a directory
     * @param path
     */
    isDir(path: string): Promise<boolean>;
    /**
     * Rename entry / Move file or directory to another directory.
     *
     * @param path Path of the actual entry
     * @param dest Destination directory. Keep it undefined if you want to keep the same directory.
     * Can be a string-path, a FileHelper instance, or a DirectoryEntry
     *
     * @param new_name New name of the entry. Keep it undefined if you want to keep the same name
     */
    mv(path: string, dest: string | undefined | FileHelper | DirectoryEntry, new_name: string | undefined): Promise<void>;
    /**
     * Copy file or directory to another directory.
     *
     * @param path Path of the actual entry
     * @param dest Destination directory. Keep it undefined if you want to keep the same directory.
     * Can be a string-path, a FileHelper instance, or a DirectoryEntry
     *
     * @param new_name New name, after copy, of the entry. Keep it undefined if you want to keep the same name
     */
    cp(path: string, dest: string | undefined | FileHelper | DirectoryEntry, new_name: string | undefined): Promise<void>;
    /**
     * Create a directory. Automatically create all the parent directories needed.
     * @param path Path to the new directory
     */
    mkdir(path: string): Promise<DirectoryEntry>;
    /**
     * Write a file containing content.
     * File and containing folder(s) are created automatically.
     * If file already exists, by default, file content are truncated and replaced by content.
     *
     * @param path Path of the file relative to working directory
     * @param content Content of the file. Can be a string, File instance, Blob or ArrayBuffer.
     * Other types will be serialized automatically using JSON.stringify()
     *
     * @param append Determine that the function should write at the end of the file.
     */
    write(path: string | FileEntry, content: any, append?: boolean): Promise<FileEntry>;
    read(path: string | FileEntry): Promise<string>;
    read(path: string | FileEntry, mode: FileHelperReadMode.json): Promise<any>;
    read(path: string | FileEntry, mode: FileHelperReadMode.array): Promise<ArrayBuffer>;
    read(path: string | FileEntry, mode: FileHelperReadMode.fileobj): Promise<File>;
    read(path: string | FileEntry, mode: FileHelperReadMode.text): Promise<string>;
    read(path: string | FileEntry, mode: FileHelperReadMode.internalURL): Promise<string>;
    read(path: string | FileEntry, mode: FileHelperReadMode.url): Promise<string>;
    read(path: string | FileEntry, mode: FileHelperReadMode.binarystr): Promise<string>;
    read(path: string | FileEntry, mode: FileHelperReadMode): Promise<any | string | ArrayBuffer | File>;
    /**
     * Read a file and parse content as JSON
     * @param path
     */
    readJSON(path: string | FileEntry): Promise<any>;
    /**
     * Returns internal URL (absolute file path to the file/dir)
     * @param path
     */
    toInternalURL(path: string | Entry): Promise<string>;
    /**
     * Read file content as base64 data URL (url that begin with data:xxx/xxxx;base64,)
     * @param path
     */
    readDataURL(path: string | FileEntry): Promise<string>;
    readAll(path: string | DirectoryEntry): Promise<string[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.json): Promise<any[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.array): Promise<ArrayBuffer[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.fileobj): Promise<File[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.text): Promise<string[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.internalURL): Promise<string[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.url): Promise<string[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode.binarystr): Promise<string[]>;
    readAll(path: string | DirectoryEntry, mode: FileHelperReadMode): Promise<any[] | string[] | ArrayBuffer[] | File[]>;
    /**
     * Get an existing file or directory using an absolute path
     * @param path Complete path
     */
    absoluteGet(path: string): Promise<Entry>;
    /**
     * Get an entry from a path.
     * If you want to read a file, use read() instead.
     * @param path Path to file or directory
     */
    get(path?: string): Promise<Entry>;
    /**
     * Create / get a file and return FileEntry of it. Autocreate need parent directories.
     * @param path Path to file
     */
    touch(path: string): Promise<FileEntry>;
    /**
     * Get content of a directory.
     *
     * @param path Path of thing to explore
     *
     * @param option_string Options, in a string. Can be e, l, d, p or f.
     * Options letters can be combined.
     * - "e": Return a `EntryObject` instead of returning file names.
     * - "l": Return an array of `FileStats` instead of returning file names.
     * - "d": Return only directories.
     * - "f": Return only files.
     * - "p": Disable directory prefixing.
     *  Remove autoprefixed names if you use a non-empty path parameter.
     *  Does not work if max_depth is not equal to 0.
     *
     * @param max_depth Recursive mode. For unlimited depth, use a negative value. Recursive exploration can be very slow.
     * max_depth mean maximum depth UNDER specified directory. 0 = Non-recursive, 1 = Current dir + one nested level, ...
     */
    ls(path?: string | DirectoryEntry, option_string?: string, max_depth?: number): Promise<string[] | FileStats[] | EntryObject>;
    /**
     * Get a tree to see files below a certain directory
     * @param path Base path for tree
     * @param mime_type Get MIME type of files instead of null
     * @param max_depth See ls() max_depth parameter
     */
    tree(path?: string | DirectoryEntry, mime_type?: boolean, max_depth?: number): Promise<EntryTree>;
    /**
     * Remove a file or a directory.
     * @param path Path of the file to remove. Do **NOT** remove root, use empty() !
     * @param r Make rm recursive.
     * If r = false and path contains a directory that is not empty, remove will fail
     */
    rm(path: string, r?: boolean): Promise<void>;
    /**
     * Remove all content of a directory.
     * If path is a file, truncate file data to empty.
     *
     * @param path Path or Entry
     * @param r Make empty recursive. (if path is a directory)
     * If r = false and path contains a directory that is not empty, empty will fail
     */
    empty(path: string | Entry, r?: boolean): Promise<void>;
    /**
     * Get information about a file
     * @param path Path to a file
     */
    stats(path: string): Promise<FileStats>;
    /**
     * Change current instance root to another directory
     * @param path **DIRECTORY** path (must NOT be a file)
     * @param relative Specify if path is relative to current directory
     */
    cd(path: string, relative?: boolean): Promise<void>;
    /**
     * Create a new FileHelper instance from a relative path of this current instance,
     * ensure that new path exists and return the FileHelper instance when its ready.
     *
     * @param relative_path Relative path from where creating the new instance
     */
    newFromCd(relative_path: string): Promise<FileHelper>;
    /**
     * Find files into a directory using a glob bash pattern.
     * @param pattern
     * @param recursive Make glob function recursive. To use ** pattern, you MUST use recursive mode.
     * @param regex_flags Add additionnal flags to regex pattern matching
     */
    glob(pattern: string, recursive?: boolean, regex_flags?: string): Promise<string[]>;
    /**
     * Get current root directory of this instance
     */
    pwd(): string;
    /**
     * Alias for pwd()
     */
    toString(): string;
    /**
     * Get entries presents in a directory via a DirectoryEntry or via directory path.
     * @param entry
     */
    entriesOf(entry: DirectoryEntry | string): Promise<Entry[]>;
    /**
     * Get entries from numerous paths
     * @param paths Array of string paths
     */
    entries(...paths: string[]): Promise<Entry[]>;
    /**
     * Get a FileEntry from a DirectoryEntry. File will be created if "filename" does not exists in DirectoryEntry
     * @param dir DirectoryEntry
     * @param filename Name a the file to get
     */
    getFileEntryOfDirEntry(dir: DirectoryEntry, filename: string): Promise<FileEntry>;
    /**
     * Get a File object from a path or a FileEntry
     * @param path String path or a FileEntry
     */
    getFile(path: string | FileEntry): Promise<File>;
    /**
     * Read a file object as selected mode
     * @param file File obj
     * @param mode Mode
     */
    readFileAs(file: File, mode?: FileHelperReadMode): Promise<any | File | string | ArrayBuffer>;
    /**
     * Get informations about a single file using a File instance
     * @param entry File
     */
    protected getStatsFromFile(entry: File): FileStats;
    /**
     * Convert string, ArrayBuffer, File, classic objects to a Blob instance
     * @param s
     */
    toBlob(s: any): Blob;
}
